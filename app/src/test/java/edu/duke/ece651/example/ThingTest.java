package edu.duke.ece651.example;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class ThingTest {
    @Test
    public void test_myFunction() {
        Thing x = new Thing(3);
        assertEquals(8, x.myFunction(1,2));
        assertEquals(42, x.myFunction(5,6));
    }
}
