package edu.duke.ece651.example;

import java.util.Enumeration;
import java.util.NoSuchElementException;

public class IntRangeEnumeration implements Enumeration<Integer> {
    private int current;
    private int max;
    public IntRangeEnumeration(int low, int high) {
        this.current = low;
        this.max = high;
    }

    @Override
    public boolean hasMoreElements() {
        return current < max;
    }

    @Override
    public Integer nextElement() {
        if (current >= max) {
            throw new NoSuchElementException("No more elements in range");
        }
        int temp = current;
        current++;
        return temp;
    }
}
