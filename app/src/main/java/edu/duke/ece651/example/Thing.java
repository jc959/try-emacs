package edu.duke.ece651.example;

import java.util.ArrayList;

public class Thing {
    private int x;
    public Thing(int x) {
        this.x = x;
    }
    public int myFunction(int a, int b) {
        if (a <= 4) {
            return 2 * a + b * x;
        } else {
            return a * x - b + 33;
        }
    }

    public int computeStuff(ArrayList<Integer> a,
                            ArrayList<Integer> b) {
        int temp = 0;
        for (int i = 0; i < Math.min(a.size(),b.size()); i++) {
            temp += a.get(i) * b.get(i);
        }
        return myFunction(a.get(0), b.get(0)) * temp;
    }

}
